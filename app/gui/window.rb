# frozen_string_literal: true

## Definition of class Window
# Serves to create a window.
class Window
  attr_reader :x, :y, :w, :elements
  attr_accessor :h

  def initialize(pos_x, pos_y, width, title)
    @x = pos_x
    @y = pos_y
    @w = width
    @h = 0
    @title = title
    @font = 'fonts/monogram.ttf'
    @elements = []
  end

  def serialize
    {
      x: @x,
      y: @y,
      w: @w,
      h: @h,
      title: @title
    }
  end

  def inspect
    serialize.to_s
  end

  def to_s
    serialize.to_s
  end

  def render_hash
    [
      { x: @x, y: @y, w: @w, h: @h, r: 255, g: 255, b: 255, a: 255 }.solid!,
      { x: @x, y: @y, w: @w, h: @h }.border!,
      { x: @x, y: (@h + @y), w: @w, h: 30, r: 0, g: 0, b: 0, a: 255 }.solid!,
      { x: @x, y: (@h + @y), w: @w, h: 1 }.border!,
      { x: (@x + 10), y: (@h + @y + 28), text: @title, r: 255, g: 255, b: 255, font: @font, size_enum: 1 }.label!
    ]
  end

  def add_element(elem)
    @elements.push(elem)
  end

end
