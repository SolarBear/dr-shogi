# frozen_string_literal: true

## Definition of class Turn
# Serves to create a turn where we can get information on the game
class Turn
  attr_reader :player
  attr_accessor :board, :move

  def initialize(player, board)
    @player = player.clone
    @board = board.map { |row| row.map(&:clone) }
    @move = nil
  end

  def run_move(move)
    @move = move
    @board[@move.destination.y][@move.destination.x] = @move.piece
    @board[@move.origin.y][@move.origin.x] = nil unless @move.origin.nil?
  end

  def serialize
    { player: @player, move: @move }
  end

  def inspect
    serialize.to_s
  end

  def to_s
    serialize.to_s
  end
end
