# frozen_string_literal: true

class ShogiPiece
  attr_reader :type, :sprite
  attr_accessor :player, :is_promoted, :theme

  MOVES = {
    lancer: { front: :infinite, back: 0, sides: 0, diag_front: 0, diag_back: 0 },
    pawn: { front: 1, back: 0, sides: 0, diag_front: 0, diag_back: 0 },
    knight: { front: 2, back: 0, sides: 1, diag_front: 0, diag_back: 0 },
    silver: { front: 1, back: 0, sides: 0, diag_front: 1, diag_back: 1 },
    gold: { front: 1, back: 1, sides: 1, diag_front: 1, diag_back: 0 },
    king: { front: 1, back: 1, sides: 1, diag_front: 1, diag_back: 1 },
    bishop: { front: 0, back: 0, sides: 0, diag_front: :infinite, diag_back: :infinite },
    horse: { front: 1, back: 1, sides: 1, diag_front: :infinite, diag_back: :infinite },
    rook: { front: :infinite, back: :infinite, sides: :infinite, diag_front: 0, diag_back: 0 },
    dragon: { front: :infinite, back: :infinite, sides: :infinite, diag_front: 1, diag_back: 1 }
  }.freeze

  INITIALS = {
    pawn: 'P',
    lancer: 'L',
    knight: 'N',
    silver: 'S',
    gold: 'G',
    king: 'K',
    bishop: 'B',
    rook: 'R'
  }.freeze

  SPRITE_CODES = {
    pawn: %w[FU TO],
    lancer: %w[KY NY],
    knight: %w[KE NK],
    silver: %w[GI NG],
    bishop: %w[KA UM],
    rook: %w[HI RY]
  }.freeze

  def initialize(type, player, is_promoted, theme = 'kanji_brown')
    @type = type
    @player = player
    @is_promoted = is_promoted
    @theme = theme
    @sprite = get_sprite
  end

  def render_hash(args, pos_x, pos_y)
    {
      x: pos_x,
      y: pos_y,
      w: args.state.tile_size,
      h: args.state.tile_size,
      path: @sprite
    }.sprite!
  end

  def promote
    unless @is_promoted
      @is_promoted = true
      @sprite = get_sprite
    end
  end

  def demote
    if @is_promoted
      @is_promoted = false
      @sprite = get_sprite
    end
  end

  def change_player(player)
    @player = player
    @is_promoted = false
    @sprite = get_sprite
  end

  def initials
    @is_promoted ? "+#{INITIALS[@type]}" : INITIALS[@type]
  end

  def serialize
    { type: @type, is_promoted: @is_promoted, player: @player }
  end

  def inspect
    serialize.to_s
  end

  def to_s
    serialize.to_s
  end

  def get_moves
    case @type
    when :pawn
      @is_promoted ? MOVES[:gold] : MOVES[:pawn]
    when :lancer
      @is_promoted ? MOVES[:gold] : MOVES[:lancer]
    when :knight
      @is_promoted ? MOVES[:gold] : MOVES[:knight]
    when :silver
      @is_promoted ? MOVES[:gold] : MOVES[:silver]
    when :gold
      MOVES[:gold]
    when :king
      MOVES[:king]
    when :bishop
      @is_promoted ? MOVES[:horse] : MOVES[:bishop]
    when :rook
      @is_promoted ? MOVES[:dragon] : MOVES[:rook]
    else
      MOVES[:gold]
    end
  end

  def get_sprite
    theme_path = "sprites/pieces/#{@theme}/"
    file_name =
      case @type
      when :king
        @player.is_jewel ? 'GY' : 'OU'
      when :gold
        'KI'
      else
        @is_promoted ? SPRITE_CODES[@type][1] : SPRITE_CODES[@type][0]
      end
    "#{theme_path}#{@player.id}#{file_name}.png"
  end

  def get_modifiers(args, board, piece_position)
    modifiers = []
    moves = get_moves

    if moves.front == 1
      modifiers.push({ x: piece_position.x, y: piece_position.y + (@player.id == 0 ? 1 : -1) })
    end
    if moves.sides == 1
      modifiers.push({ x: piece_position.x + 1, y: piece_position.y })
      modifiers.push({ x: piece_position.x - 1, y: piece_position.y })
    end
    if moves.back == 1
      modifiers.push({ x: piece_position.x, y: piece_position.y + (@player.id == 0 ? -1 : 1) })
    end
    if moves.diag_front == 1
      modifiers.push({ x: piece_position.x + 1, y: piece_position.y + (@player.id == 0 ? 1 : -1) })
      modifiers.push({ x: piece_position.x - 1, y: piece_position.y + (@player.id == 0 ? 1 : -1) })
    end
    if moves.diag_back == 1
      modifiers.push({ x: piece_position.x + 1, y: piece_position.y + (@player.id == 0 ? -1 : 1) })
      modifiers.push({ x: piece_position.x - 1, y: piece_position.y + (@player.id == 0 ? -1 : 1) })
    end
    if moves.diag_front == :infinite && moves.diag_back == :infinite
      found_nw = false
      found_ne = false
      found_sw = false
      found_se = false
      (1..args.state.grid_w - 1).each do |i|
        if (piece_position.x + i) < args.state.grid_w && (piece_position.y + i) < args.state.grid_h && !found_ne
          modifiers.push({ x: piece_position.x + i, y: piece_position.y + i })
          found_ne = true unless board[piece_position.y + i][piece_position.x + i].nil?
        end
        if (piece_position.x - i) >= 0 && (piece_position.y + i) < args.state.grid_h && !found_nw
          modifiers.push({ x: piece_position.x - i, y: piece_position.y + i })
          found_nw = true unless board[piece_position.y + i][piece_position.x - i].nil?
        end
        if (piece_position.x + i) < args.state.grid_w && (piece_position.y - i) >= 0 && !found_se
          modifiers.push({ x: piece_position.x + i, y: piece_position.y - i })
          found_se = true unless board[piece_position.y - i][piece_position.x + i].nil?
        end
        if (piece_position.x - i) >= 0 && (piece_position.y - i) >= 0 && !found_sw
          modifiers.push({ x: piece_position.x - i, y: piece_position.y - i })
          found_sw = true unless board[piece_position.y - i][piece_position.x - i].nil?
        end
      end
    end
    if moves.sides == :infinite
      found_n = false
      found_s = false
      found_w = false
      found_e = false
      (1..args.state.grid_w - 1).each do |i|
        if (piece_position.y + i) < args.state.grid_h && !found_n
          modifiers.push({ x: piece_position.x, y: piece_position.y + i })
          found_n = true unless board[piece_position.y + i][piece_position.x].nil?
        end
        if (piece_position.y - i) >= 0 && !found_s
          modifiers.push({ x: piece_position.x, y: piece_position.y - i })
          found_s = true unless board[piece_position.y - i][piece_position.x].nil?
        end
        if (piece_position.x - i) >= 0 && !found_w
          modifiers.push({ x: piece_position.x - i, y: piece_position.y })
          found_w = true unless board[piece_position.y][piece_position.x - i].nil?
        end
        if (piece_position.x + i) < args.state.grid_w && !found_e
          modifiers.push({ x: piece_position.x + i, y: piece_position.y })
          found_e = true unless board[piece_position.y][piece_position.x + i].nil?
        end
      end
    end
    if moves.front == :infinite && moves.back == 0
      found = false
      (1..args.state.grid_h - 1).each do |i|
        if (piece_position.y + (@player.id == 0 ? i : -i)) >= 0 &&
           (piece_position.y + (@player.id == 0 ? i : -i)) < args.state.grid_h && !found
          modifiers.push({ x: piece_position.x, y: piece_position.y + (@player.id == 0 ? i : -i) })
          found = true unless board[piece_position.y + (@player.id == 0 ? i : -i)][piece_position.x].nil?
        end
      end
    end
    if moves.front == 2 && moves.sides == 1
      modifiers = [
        { x: piece_position.x - 1, y: piece_position.y + (@player.id == 0 ? 2 : -2) },
        { x: piece_position.x + 1, y: piece_position.y + (@player.id == 0 ? 2 : -2) }
      ]
    end
    check_modifiers(args, board, piece_position, modifiers)
  end

  def check_modifiers(args, board, piece_position, modifiers)
    checked_positions = []
    piece = get_tile(args, board, piece_position.x, piece_position.y)
    modifiers.each do |mod|
      tile = get_tile(args, board, mod.x, mod.y)
      if (tile.nil? || tile&.player.id != piece&.player.id) &&
         check_position_in_bounds(args, mod.x, mod.y)
        checked_positions.push(mod)
      end
    end
    checked_positions
  end

  def get_tile(args, board, pos_x, pos_y)
    board[pos_y][pos_x] if check_position_in_bounds(args, pos_x, pos_y)
  end

  def check_position_in_bounds(args, pos_x, pos_y)
    pos_x >= 0 &&
      pos_x < args.state.grid_w &&
      pos_y >= 0 &&
      pos_y < args.state.grid_h
  end

end
