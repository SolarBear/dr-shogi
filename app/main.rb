# frozen_string_literal: true

# GUI
require 'app/gui/window_element.rb'
require 'app/gui/window.rb'
require 'app/gui/button.rb'
require 'app/gui/textbox.rb'
require 'app/gui/checkbox.rb'
require 'app/gui/confirm_dialog.rb'
require 'app/gui/scroll_panel.rb'

# Shogi
require 'app/shogi_piece.rb'
require 'app/player.rb'
require 'app/board.rb'
require 'app/camp.rb'
require 'app/turn.rb'
require 'app/move.rb'

# Game
require 'app/game.rb'
require 'app/tick.rb'
