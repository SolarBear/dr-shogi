# frozen_string_literal: true

class Board
  attr_reader :game

  def initialize(args, width, height, type = :default, theme = 'tile_wood1')
    @theme = theme
    @w = width
    @h = height
    @type = type
    @game = init_board(args)
  end

  def serialize
    {
      w: @w,
      h: @h,
      type: @type,
      theme: @theme
    }
  end

  def inspect
    serialize.to_s
  end

  def to_s
    serialize.to_s
  end

  def init_board(args)
    [
      [ # row 0
        ShogiPiece.new(:lancer, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:knight, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:silver, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:gold, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:king, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:gold, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:silver, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:knight, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:lancer, args.state.players[0], false, args.state.theme)
      ],
      [ # row 1
        nil,
        ShogiPiece.new(:bishop, args.state.players[0], false, args.state.theme),
        nil,
        nil,
        nil,
        nil,
        nil,
        ShogiPiece.new(:rook, args.state.players[0], false, args.state.theme),
        nil
      ],
      [ # row 2
        ShogiPiece.new(:pawn, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[0], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[0], false, args.state.theme)
      ],
      [nil, nil, nil, nil, nil, nil, nil, nil, nil],
      [nil, nil, nil, nil, nil, nil, nil, nil, nil],
      [nil, nil, nil, nil, nil, nil, nil, nil, nil],
      [ # row 6
        ShogiPiece.new(:pawn, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:pawn, args.state.players[1], false, args.state.theme)
      ],
      [ # row 7
        nil,
        ShogiPiece.new(:rook, args.state.players[1], false, args.state.theme),
        nil,
        nil,
        nil,
        nil,
        nil,
        ShogiPiece.new(:bishop, args.state.players[1], false, args.state.theme),
        nil
      ],
      [ # row 8
        ShogiPiece.new(:lancer, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:knight, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:silver, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:gold, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:king, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:gold, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:silver, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:knight, args.state.players[1], false, args.state.theme),
        ShogiPiece.new(:lancer, args.state.players[1], false, args.state.theme)
      ]
    ]
  end

  def sprite_path
    "sprites/boards/#{@theme}.png"
  end
end
