# frozen_string_literal: true

## Definition of class WindowElement
# Parent class for elements that can be added to windows
class WindowElement
  attr_accessor :x, :y, :w, :h

  def initialize(pos_x, pos_y, width, height)
    @x = pos_x
    @y = pos_y
    @w = width
    @h = height
  end

  def serialize
    {
      x: @x,
      y: @y,
      w: @w,
      h: @h,
      type: type
    }
  end

  def inspect
    serialize.to_s
  end

  def to_s
    serialize.to_s
  end

  def type
    'element'
  end
end
