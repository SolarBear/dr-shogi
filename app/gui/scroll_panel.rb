# frozen_string_literal: true

## Definition of class ScrollPanel
# Serves to create a scroll panel for long lists.
class ScrollPanel < WindowElement
  attr_accessor :height_offset, :content

  def initialize(pos_x, pos_y, width, height, content = [])
    super(pos_x, pos_y, width, height)
    @font = 'fonts/monogram.ttf'
    @content = content
    @height_offset = 5
  end

  def serialize
    {
      x: @x,
      y: @y,
      w: @w,
      h: @h,
      content: @content
    }
  end

  def inspect
    serialize.to_s
  end

  def to_s
    serialize.to_s
  end

  def render_hash
    render_hash = [
      { x: @x, y: @y, w: @w, h: @h, r: 50, g: 50, b: 50, a: 200 }.solid!,
      { x: @x, y: @y, w: @w, h: @h }.border!
    ]
    @content.reverse.each_with_index do |elem, idx|
      move_height = (@y + @h) - (25 * idx) - @height_offset
      next unless move_height < (@y + @h) && move_height > (@y + 25)

      count = @content.size - idx
      render_hash.push(
        {
          x: @x + 10, y: move_height, w: @w - 20,
          text: "#{count < 10 ? '00' : (count < 100 ? '0' : '')}#{count}", font: @font, r: 255, g: 255, b: 255
        }.label!
      )
      render_hash.push(
        {
          x: @x + 60, y: move_height, w: @w - 60,
          text: elem, font: @font, r: 255, g: 255, b: 255
        }.label!
      )
    end
    render_hash
  end

  def type
    'scroll_panel'
  end

end
