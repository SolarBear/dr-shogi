# frozen_string_literal: true

## Definition of class Textbox
# Serves to create a textbox for menus and windows.
# Source: https://github.com/Islacrusez/DragonRuby-Textboxes
class Textbox < WindowElement
  attr_reader :text
  attr_accessor :text_size

  def initialize(pos_x, pos_y, width, text)
    @text_size = 1
    @font = 'fonts/monogram.ttf'
    @text = text
    # Gets maximum height of any given line from the given string
    @height_offset = get_height(@text, @text_size, @font)
    @lines = string_to_lines(@text, width)
    super(pos_x, pos_y, width, (@height_offset * @lines.size))
  end

  def render_hash
    if @lines.is_a?(String)
      # Returns early if the text is too short to split
      @output = [{ x: @x, y: @y, text: @text, size_enum: @text_size, font: @font }.label!]
    else
      @lines.map!.with_index do |line, idx|
        { x: @x, y: @y - (idx * @height_offset), text: line, size_enum: @text_size, font: @font }.label!
      end
      @output = @lines
    end
    @output
  end

  # Internal method utilising calcstringbox to return string box length
  def get_length(string, size = 0, font = 'default')
    $gtk.args.gtk.calcstringbox(string, size, font).x
  end

  # Internal method utilising calcstringbox to return string box height
  def get_height(string, size = 0, font = 'default')
    $gtk.args.gtk.calcstringbox(string, size, font).y
  end

  def string_to_lines(string, box_x, size = 0, font = 'default')
    return string unless get_length(string, size, font) > box_x

    # Removes carriage returns, leaving only line breaks
    string.gsub!("\r", '')
    # splits string into array at linebreak
    strings_with_linebreaks = string.split("\n")
    list_of_strings = strings_with_linebreaks.map do |line|
      # Ignores blank strings, as caused by consecutive linebreaks
      next if line == ''

      line.split # Splits strings into arrays of words at any whitespace
      # Results in nested array, [[],[]]!
    end

    list_to_lines(list_of_strings, box_x, size, font)
  end

  def list_to_lines(strings, box_x, size, font)
    line = ''                                                   # Define string
    lines = []                                                  # Define array
    # Collapses nested arrays into one array, and removes the trailing blank 'word'
    strings.map! do |string|
      next unless string                                      # Handles Nil entries from multiple newlines

      string << ''                                            # Adds a blank 'word' to the end of each outer array, to trigger newline code
    end.flatten!.pop
    strings.each do |word|
      if word.empty? || !word # Handling of blank 'words' and Nil entries in arrays
        lines.push line.dup unless line.empty?              # Adds existing accumulated words to the current line
        lines.push ' ' if line.empty?                       # Adds a space if no words accrued
        line.clear                                          # Clears the accumulator
      elsif get_length("#{line} #{word}", size, font) <= box_x # "If current word fits on the end of the current line, do"
        line << ' ' if line.length.positive? # Inserts a space into accumulator if the line isn't blank
        line << word # Adds the current word to the accumulator
      else # "If the word doesn't fit, instead do"
        lines.push line.dup                                 # Adds accumulator to current line
        line.clear                                          # Clears accumulator
        line << word                                        # Adds current word to accumulator
      end
    end                                                         # Once all words in all strings are processed
    lines.push line.dup                                         # Add accumulator to current line, as it's possible for accumulator to not have been committed
    lines                                                # Return array of lines, explicitly to be safe.
  end

  def type
    'textbox'
  end
end
