# frozen_string_literal: true

## Definition of class Move
# Serves to create and validate a player movement of a piece
# Follows the notation described here: https://en.wikipedia.org/wiki/Shogi_notation
class Move
  attr_reader :piece, :origin, :movement, :destination
  attr_accessor :promotion

  def initialize(piece, origin, movement, destination, promotion = '')
    @piece = piece.clone
    @origin = origin
    @movement = movement
    @destination = destination
    @promotion = promotion
  end

  def to_s
    "#{@piece.initials}#{@origin.nil? ? '' : "#{9 - @origin.x}#{9 - @origin.y}"}#{@movement}#{"#{9 - @destination.x}#{9 - @destination.y}"}#{@promotion}"
  end

  def serialize
    { piece: @piece, origin: @origin, movement: @movement, destination: @destination, promotion: @promotion }
  end

  def inspect
    serialize.to_s
  end

  def is_valid?(args)
    is_valid = false

    mods = @piece.get_modifiers(args, args.state.game_board, (@origin.nil? ? @destination : @origin))
    tile = args.state.game_board[@destination.y][@destination.x]

    return true if !args.state.option_validation && @piece.player.id != tile&.player.id

    case @movement
    when '-'
      is_valid = true if tile.nil? && mods.find { |mod| mod == @destination }
    when 'x'
      is_valid = true if tile.player.id != args.state.current_player &&
                         mods.find { |mod| mod == @destination }
    when '*'
      is_valid = true if tile.nil? &&
                         !(
                           @piece.type == :pawn &&
                           (
                             mods.find { |mod| mod == get_king_position(args.state.current_player, args.state.game_board) } ||
                             check_column_for_pawn(@piece.player.id, @destination.x, args.state.game_board) ||
                             mods.empty?
                           )
                         )
    end
    is_valid
  end

  def get_king_position(player, board)
    board.map_2d do |pos_y, pos_x, piece|
      return { x: pos_x, y: pos_y } if piece&.type == :king && piece&.player.id == player
    end
  end

  def check_column_for_pawn(player, column, board)
    board.map_2d do |pos_y, pos_x, piece|
      return true if !piece.nil? &&
                     pos_x == column &&
                     (
                       piece.type == :pawn &&
                       !piece.is_promoted &&
                       piece.player.id == player
                     )
    end
    false
  end

end
