class Game
  attr_gtk

  def tick
    if args.tick_count.zero?
      init
      args.gtk.log_level = :off
    end
    if state.restart
      restart
      state.restart = false
    end
    render
    input unless state.checkmate
    # calc
  end

  def restart
    # Players
    state.current_player = 0
    state.players = [
      Player.new(0, true),
      Player.new(1, false)
    ]

    # Board
    state.board_object = Board.new(
      args,
      state.grid_w,
      state.grid_h,
      state.board_theme
    )
    state.game_board = state.board_object.game

    state.camps = [
      Camp.new(
        args,
        state.board_pos_y - state.tile_size - 5,
        state.players[0]
      ),
      Camp.new(
        args,
        (state.board_pos_y + state.board_h) + 5,
        state.players[1]
      )
    ]

    # Show menus toggle
    state.show_menu = false
    state.show_options = false
    state.show_credits = false

    # Game values
    state.selected_tile = { x: -1, y: -1 }
    state.selected_piece = nil
    state.moving_piece = false
    state.selected_camp_tile = { player_id: -1, pos: -1 }
    state.camp_selection = nil
    state.promotion_candidate = nil

    # Game toggles
    state.checkmate = false
    state.can_promote = false
    state.restart = true

    # Turns
    state.turns = []
    state.move_list = ScrollPanel.new(
      state.board_pos_x + state.board_w + 10,
      state.board_pos_y,
      180,
      state.board_h
    )

    render_once
    render_update
  end

  def init
    # Visuals
    state.theme ||= 'kanji_brown'
    state.board_theme ||= 'tile_wood1'
    state.font ||= 'fonts/monogram.ttf'

    # Player
    state.jewel_king_player ||= 0

    # Board
    state.tile_size ||= 64
    state.grid_w ||= 9
    state.grid_h ||= 9
    state.board_pos_x ||= 352
    state.board_pos_y ||= 72
    state.board_h ||= state.grid_h * state.tile_size
    state.board_w ||= state.grid_w * state.tile_size
    state.tiles ||= []

    # Game toggles
    state.option_highlight = true
    state.option_validation = true
    state.option_threat_map = false

    # Other
    state.tick_delay = 0

    # Trigger a restart
    state.restart = true
  end

  def render_once
    render_target_board
    render_target_player_tags
  end

  def render_update
    render_target_pieces
    render_target_threat_map
  end

  def render
    outputs.background_color = [70, 70, 70]
    render_current_player_background
    render_board
    render_player_tags
    render_threat_map if state.option_threat_map
    unless state.show_menu ||
           state.show_options ||
           state.show_credits ||
           state.can_promote ||
           state.checkmate
      render_possible_moves if state.option_highlight
      render_hover_selector
      select_tile
      render_click_selector
      select_camp_tile
    end
    render_check_selector if is_king_checked(state.current_player, state.game_board)
    render_pieces
    render_camps
    render_move_list
    unless state.show_menu ||
           state.show_options ||
           state.show_credits
      render_promotion_dialog
    end
    render_end_screen if state.checkmate
    render_menu
    render_options
    render_credits
  end

  def input
    handle_key_events
    handle_scroll_move_list
  end

  def delay_activation(frame_num)
    state.tick_delay = args.tick_count + frame_num
  end

  def check_delay_activation
    args.tick_count >= state.tick_delay
  end

  # Board
  # -----------------------------------------------------------------------------
  def render_target_board
    tiles = []
    (0..state.grid_h - 1).each do |pos_y|
      (0..state.grid_w - 1).each do |pos_x|
        tiles.push(
          {
            x: state.board_pos_x + (pos_x * state.tile_size),
            y: state.board_pos_y + (pos_y * state.tile_size),
            w: state.tile_size,
            h: state.tile_size
          }.border!
        )
      end
    end
    state.tiles = tiles
    outputs[:board].primitives << [
      {
        x: state.board_pos_x,
        y: state.board_pos_y,
        w: state.board_w,
        h: state.board_h,
        path: state.board_object.sprite_path
      }.sprite!,
      state.tiles,
      {
        x: state.board_pos_x - 1,
        y: state.board_pos_y - 1,
        w: state.board_w + 2,
        h: state.board_h + 2
      }.border!
    ]
  end

  def render_board
    outputs.primitives << { x: 0, y: 0, w: grid.w, h: grid.h, path: :board }.sprite!
  end

  # Pieces
  # -----------------------------------------------------------------------------
  def render_target_pieces
    pieces = []
    state.game_board.map_2d do |pos_y, pos_x, value|
      unless value.nil?
        pieces.push(
          value.render_hash(
            args,
            state.board_pos_x + (pos_x * state.tile_size),
            state.board_pos_y + (pos_y * state.tile_size)
          )
        )
      end
    end
    outputs[:pieces].primitives.clear
    outputs[:pieces].primitives << pieces
  end

  def render_pieces
    outputs.primitives << { x: 0, y: 0, w: grid.w, h: grid.h, path: :pieces }.sprite!
  end

  def render_hover_selector
    grid_position = calc_current_hover
    if grid_position.x >= 0 && grid_position.y >= 0
      tile = grid_position.tile
      outputs.primitives << {
        x: tile.x + 1,
        y: tile.y + 1,
        w: tile.w - 2,
        h: tile.h - 2,
        r: 255, g: 255, b: 255, a: 100
      }.solid!
    end
  end

  def render_check_selector
    king_position = get_king_position(state.current_player, state.game_board)
    outputs.primitives << {
      x: state.board_pos_x + (king_position.x * state.tile_size) + 1,
      y: state.board_pos_y + (king_position.y * state.tile_size) + 1,
      w: state.tile_size - 2,
      h: state.tile_size - 2,
      r: 255, g: 109, b: 106, a: 100
    }.solid!
  end

  def render_click_selector
    if state.selected_tile.x >= 0 &&
       state.selected_tile.y >= 0 &&
       (
         !state.game_board[state.selected_tile.y][state.selected_tile.x].nil? &&
          check_selected_tile_in_bounds
       )
      outputs.primitives << {
        x: state.board_pos_x + (state.selected_tile.x * state.tile_size) + 1,
        y: state.board_pos_y + (state.selected_tile.y * state.tile_size) + 1,
        w: state.tile_size - 2,
        h: state.tile_size - 2,
        r: 255, g: 255, b: 255, a: 100
      }.solid!
    end
  end

  def render_possible_moves
    piece = state.selected_piece
    unless piece.nil?
      modifiers = piece.get_modifiers(args, state.game_board, state.selected_tile)
      modifier_primitives = []
      modifiers&.each do |mod|
        is_enemy = false
        tile = get_tile(mod.x, mod.y)
        is_enemy = true if !tile.nil? && tile&.player != state.selected_piece.player
        # red: RGB(255,109,106)
        # blue: RGB(139,211,230)
        modifier_primitives.push(
          {
            x: state.board_pos_x + (mod.x * state.tile_size) + 1,
            y: state.board_pos_y + (mod.y * state.tile_size) + 1,
            w: state.tile_size - 2,
            h: state.tile_size - 2,
            r: is_enemy ? 255 : 139,
            g: is_enemy ? 109 : 211,
            b: is_enemy ? 106 : 230,
            a: 150
          }.solid!
        )
      end
      args.outputs.primitives << modifier_primitives
    end
  end

  def render_target_threat_map
    threat_map = get_threat_map(state.current_player, state.game_board)
    threat_primitives = []
    threat_map.each do |selector|
      is_enemy = false
      tile = get_tile(selector.x, selector.y)
      is_enemy = true if !tile.nil? && tile&.player == state.current_player
      # red: RGB(255,109,106)
      # blue: RGB(139,211,230)
      threat_primitives.push(
        {
          x: state.board_pos_x + (selector.x * state.tile_size) + 1,
          y: state.board_pos_y + (selector.y * state.tile_size) + 1,
          w: state.tile_size - 2,
          h: state.tile_size - 2,
          r: is_enemy ? 255 : 139,
          g: is_enemy ? 109 : 211,
          b: is_enemy ? 106 : 230,
          a: 150
        }.solid!
      )
    end
    outputs[:threat_map].primitives.clear
    outputs[:threat_map].primitives << threat_primitives
  end

  def render_threat_map
    outputs.primitives << { x: 0, y: 0, w: grid.w, h: grid.h, path: :threat_map }.sprite!
  end

  def get_threat_map(player, board)
    enemy_pieces_moves = []
    board.map_2d do |pos_y, pos_x, piece|
      if !piece.nil? && (piece.player.id != player)
        enemy_pieces_moves =
          enemy_pieces_moves.union(
            piece.get_modifiers(args, board, { x: pos_x, y: pos_y })
          )
      end
    end
    enemy_pieces_moves.uniq
  end

  def is_king_checked(player, board)
    threat_map = get_threat_map(player, board)
    king_position = get_king_position(player, board)
    !(threat_map.find { |threat| threat == king_position }).nil?
  end

  def is_checkmate(player_id)
    count = 0
    state.game_board.map_2d do |pos_y, pos_x, piece|
      if piece&.player.id == player_id
        mods = piece.get_modifiers(args, state.game_board, { x: pos_x, y: pos_y })
        mods.each do |mod|
          if state.game_board[mod.y][mod.x].nil?
            move = Move.new(
              piece,
              { x: pos_x, y: pos_y },
              '-',
              { x: mod.x, y: mod.y }
            )
            turn = Turn.new(state.players[player_id], state.game_board)
            turn.run_move(move)
            count += 1 if move.is_valid?(args) && !is_king_checked(player_id, turn.board)
          elsif state.game_board[mod.y][mod.x].player.id != player_id
            move = Move.new(
              piece,
              { x: pos_x, y: pos_y },
              'x',
              { x: mod.x, y: mod.y }
            )
            turn = Turn.new(state.players[player_id], state.game_board)
            turn.run_move(move)
            count += 1 if move.is_valid?(args) && !is_king_checked(player_id, turn.board)
          end
        end
      elsif piece.nil?
        state.players[player_id].captured_pieces.each do |cap_piece|
          move = Move.new(
            cap_piece,
            nil,
            '*',
            { x: pos_x, y: pos_y }
          )
          turn = Turn.new(state.players[player_id], state.game_board)
          turn.run_move(move)
          count += 1 if move.is_valid?(args) && !is_king_checked(player_id, turn.board)
        end
      end
    end
    count.zero?
  end

  def get_king_position(player, board)
    board.map_2d do |pos_y, pos_x, piece|
      return { x: pos_x, y: pos_y } if piece&.type == :king && piece&.player.id == player
    end
  end

  def calc_current_hover
    grid_position = { x: -1, y: -1 }
    state.tiles.each do |tile|
      next unless inputs.mouse.inside_rect?(tile)

      grid_position = {
        x: ((tile.x - state.board_pos_x) / state.tile_size).to_i,
        y: ((tile.y - state.board_pos_y) / state.tile_size).to_i,
        tile: tile
      }
      break
    end
    grid_position
  end

  def get_tile(pos_x, pos_y)
    state.game_board[pos_y][pos_x] if check_position_in_bounds(pos_x, pos_y)
  end

  def select_tile
    grid_position = calc_current_hover
    return unless inputs.mouse.click && check_position_in_bounds(grid_position.x, grid_position.y)

    piece = state.game_board[grid_position.y][grid_position.x]
    if !piece.nil? && state.selected_tile == { x: -1, y: -1 } && !state.moving_piece
      # If tile is not empty and not selected, select it.
      state.selected_tile = grid_position
      state.selected_piece = piece
      Game.unselect_camp(args)
      state.moving_piece = true
    elsif !state.camp_selection.nil? &&
          state.selected_camp_tile != { player_id: -1, pos: -1 } &&
          state.selected_camp_tile.player_id == state.current_player &&
          state.game_board[grid_position.y][grid_position.x].nil? &&
          state.moving_piece
      player = state.players[state.current_player]
      move = Move.new(
        player.captured_pieces[player.select_capture_piece(state.camp_selection.piece.type)],
        nil,
        '*',
        { x: grid_position.x, y: grid_position.y }
      )
      turn = Turn.new(state.players[state.current_player], state.game_board)
      turn.run_move(move)
      if move.is_valid?(args) && !is_king_checked(state.current_player, turn.board)
        state.players[state.current_player].drop(move.piece.type)
        state.game_board = turn.board
        Game.unselect_camp(args)
        next_turn(turn)
      end
    elsif !piece.nil? && state.selected_tile != { x: -1, y: -1 } &&
          state.moving_piece && state.selected_piece.player.id == state.current_player &&
          state.game_board[grid_position.y][grid_position.x].player.id != state.current_player
      move = Move.new(
        state.selected_piece,
        state.selected_tile,
        'x',
        { x: grid_position.x, y: grid_position.y }
      )
      turn = Turn.new(state.players[state.current_player], state.game_board)
      turn.run_move(move)
      if move.is_valid?(args) && !is_king_checked(state.current_player, turn.board)
        state.players[state.current_player].capture(state.game_board[grid_position.y][grid_position.x])
        state.game_board = turn.board
        handle_promotion(state.selected_tile.y, grid_position.x, grid_position.y)
        next_turn(turn)
      end
    elsif piece.nil? && state.moving_piece && state.selected_piece.player.id == state.current_player
      # If target tile is empty and a piece is selected, move the piece to the tile
      move = Move.new(
        state.selected_piece,
        state.selected_tile,
        '-',
        { x: grid_position.x, y: grid_position.y }
      )
      turn = Turn.new(state.players[state.current_player], state.game_board)
      turn.run_move(move)
      if move.is_valid?(args) && !is_king_checked(state.current_player, turn.board)
        state.game_board = turn.board
        handle_promotion(state.selected_tile.y, grid_position.x, grid_position.y)
        next_turn(turn)
      end
    elsif state.selected_tile != { x: -1, y: -1 } && state.moving_piece
      # If you click again on an already selected piece, unselect it.
      Game.unselect_tile(args)
    end
    render_update
  end

  def print_board(board)
    board.each do |line|
      str_line = ''
      line.each do |col|
        str_line += "#{col.nil? ? '_' : col&.initials} "
      end
      puts str_line
    end
  end

  def self.unselect_camp(args)
    args.state.selected_camp_tile = { player_id: -1, pos: -1 }
    args.state.camp_selection = nil
    args.state.moving_piece = false
  end

  def self.unselect_tile(args)
    args.state.selected_tile = { x: -1, y: -1 }
    args.state.selected_piece = nil
    args.state.moving_piece = false
  end

  def handle_promotion(prev_pos_y, pos_x, pos_y)
    if (
         (
           (state.selected_piece.player.id == 0 && pos_y >= (state.grid_h - 3) && pos_y < state.grid_h) ||
           (state.selected_piece.player.id == 1 && pos_y >= (0) && pos_y < 3)
         ) || (
           (state.selected_piece.player.id == 0 && prev_pos_y >= (state.grid_h - 3) && prev_pos_y < state.grid_h) ||
           (state.selected_piece.player.id == 1 && prev_pos_y >= (0) && prev_pos_y < 3)
         )
       ) && !state.selected_piece.is_promoted &&
       (state.selected_piece.type != :king && state.selected_piece.type != :gold)
      state.can_promote = true
      state.promotion_candidate = {
        x: pos_x,
        y: pos_y
      }
    end
    state.moving_piece = true
    Game.unselect_tile(args)
  end

  def check_position_in_bounds(pos_x, pos_y)
    pos_x >= 0 &&
      pos_x < state.grid_w &&
      pos_y >= 0 &&
      pos_y < state.grid_h
  end

  def check_selected_tile_in_bounds
    state.selected_tile.y >= 0 &&
      state.selected_tile.x >= 0 &&
      state.selected_tile.y < state.grid_h &&
      state.selected_tile.x < state.grid_w
  end

  def next_turn(turn)
    state.turns.push(turn)
    state.checkmate = is_checkmate(state.current_player == 0 ? 1 : 0)
    state.current_player = (state.current_player == 0 ? 1 : 0) unless state.checkmate
  end

  # Player tags
  # -----------------------------------------------------------------------------
  def render_target_player_tags
    player0_tag = [
      { x: 5, y: 5, w: 200, h: 50, r: 0, g: 0, b: 0, a: 255 }.solid!,
      { x: 10, y: 10, w: 200 - 10, h: 50 - 10, r: 255, g: 255, b: 255, a: 255 }.border!,
      {
        x: 105, y: 45, text: 'Player 1',
        alignment_enum: 1,
        size_enum: 3,
        r: 255, g: 255, b: 255, a: 255,
        font: state.font
      }.label!
    ]
    player1_tag = [
      { x: 5, y: 665, w: 200, h: 50, r: 0, g: 0, b: 0, a: 255 }.solid!,
      { x: 10, y: 670, w: 200 - 10, h: 50 - 10, r: 255, g: 255, b: 255, a: 255 }.border!,
      {
        x: 105, y: 705, text: 'Player 2',
        alignment_enum: 1,
        size_enum: 3,
        r: 255, g: 255, b: 255, a: 255,
        font: state.font
      }.label!
    ]
    state.player_tags = [player0_tag, player1_tag]
    outputs[:player_tags].primitives << state.player_tags
  end

  def render_player_tags
    player = state.player_tags[state.current_player]
    outputs.primitives << [
      { x: 0, y: 0, w: grid.w, h: grid.h, path: :player_tags }.sprite!,
      { x: 22, y: player[0].y + 15, w: 20, h: 20, r: 255, g: 109, b: 106, a: 255 }.solid!
    ]
  end

  # Current player background
  # -----------------------------------------------------------------------------
  def render_current_player_background
    outputs.primitives << {
      x: 0,
      y: state.current_player.zero? ? 0 : (grid.h / 2).to_i,
      w: grid.w,
      h: (grid.h / 2).to_i,
      r: 85, g: 85, b: 85, a: 255
    }.solid!
  end

  # Camp
  # -----------------------------------------------------------------------------
  def render_camps
    outputs.primitives << state.camps.map { |camp| camp.render_hash(args) }
  end

  def select_camp_tile
    state.camps.each do |camp|
      camp.render_hash(args).each_with_index do |slot, idx|
        next unless inputs.mouse.inside_rect?(slot[0])

        if inputs.mouse.click && state.selected_camp_tile != { player_id: -1, pos: -1 }
          Game.unselect_camp(args)
        elsif inputs.mouse.click && camp.player.capture_count(camp.layout[idx].type).positive?
          piece_pos = camp.player.select_capture_piece(camp.layout[idx].type)
          state.selected_camp_tile = { player_id: camp.player.id, pos: idx }
          state.camp_selection = { pos: piece_pos, piece: camp.player.captured_pieces[piece_pos] }
          Game.unselect_tile(args)
          state.moving_piece = true
        end
      end
    end
  end

  # Move list
  # -----------------------------------------------------------------------------
  def render_move_list
    state.move_list.content = state.turns.map { |turn| turn.move.to_s }
    outputs.primitives << state.move_list.render_hash
  end

  def handle_scroll_move_list
    return unless inputs.mouse.inside_rect?(state.move_list.render_hash[0])

    if ((state.turns.size * 25) + 10) > state.move_list.h
      if !inputs.mouse.wheel.nil? &&
         ((inputs.mouse.wheel.y * 10).to_i + state.move_list.height_offset) <= 5 &&
         ((inputs.mouse.wheel.y * 10).to_i + state.move_list.height_offset) >= ((state.turns.size * 25 * -1) - 20 + state.move_list.h)
        state.move_list.height_offset += (inputs.mouse.wheel.y * 10)
      end
    else
      state.move_list.height_offset = 5
    end
  end

  # Menu
  # -----------------------------------------------------------------------------
  def render_menu
    if state.show_menu
      menu_window = Window.new(530, 300, 220, 'Menu')
      menu_elements = [
        Button.new(10, 10, 200, 30, 'Resume'),
        Button.new(10, 10, 200, 30, 'Options'),
        Button.new(10, 10, 200, 30, 'Restart'),
        Button.new(10, 10, 200, 30, 'Credits'),
        Button.new(10, 10, 200, 30, 'Exit')
      ]
      menu_elements.each do |elem|
        menu_window.h += elem.h + 5
      end
      menu_window.h += 15
      pos_y = 0
      menu_elements.each_with_index do |elem, i|
        elem.x += menu_window.x
        elem.y = (menu_window.y + menu_window.h) - pos_y - 40
        handle_menu_actions(elem) if elem.type == 'button'
        menu_window.add_element(elem)
        pos_y += elem.h + 5 if i.zero?
        pos_y += elem.h + 5 if i.positive?
      end
      args.outputs.primitives << menu_window.render_hash
      args.outputs.primitives << menu_elements.map(&:render_hash)
    end
  end

  def render_options
    if state.show_options
      options_window = Window.new(505, 330, 270, 'Options')
      options_elements = [
        Checkbox.new(10, 10, 250, 30, 'Moves highlight', args.state.option_highlight),
        Checkbox.new(10, 10, 250, 30, 'Validate movements', args.state.option_validation),
        Checkbox.new(10, 10, 250, 30, 'Show threat map', args.state.option_threat_map),
        Button.new(10, 10, 250, 30, 'Back')
      ]
      options_elements.each do |elem|
        options_window.h += elem.h + 5
      end
      options_window.h += 15
      pos_y = 0
      options_elements.each_with_index do |elem, i|
        elem.x += options_window.x
        elem.y = (options_window.y + options_window.h) - pos_y - 40
        handle_options_actions(elem) if elem.type == 'button' || elem.type == 'checkbox'
        options_window.add_element(elem)
        pos_y += elem.h + 5 if i.zero?
        pos_y += elem.h + 5 if i.positive?
      end
      args.outputs.primitives << options_window.render_hash
      args.outputs.primitives << options_elements.map(&:render_hash)
    end
  end

  def render_credits
    if state.show_credits
      credits_window = Window.new(370, 300, 540, 'Credits')
      credits_text = "Made by computer045\n \n" \
    "The sprites of the pieces and the boards have been taken from this github repository: https://github.com/Ka-hu/shogi-pieces\n \n" \
    'The Monogram font has been taken from: https://datagoblin.itch.io/monogram'
      credits_elements = [
        Textbox.new(10, 10, 480, credits_text),
        Button.new(10, 10, 200, 30, 'Back')
      ]
      credits_elements.each do |elem|
        credits_window.h += elem.h + 5
      end
      credits_window.h += 15
      pos_y = 0
      credits_elements.each_with_index do |elem, i|
        elem.x += credits_window.x
        elem.y = (credits_window.y + credits_window.h) - pos_y - 5
        handle_credits_actions(elem) if elem.type == 'button'
        credits_window.add_element(elem)
        pos_y += elem.h + 40 if i.zero?
        pos_y += elem.h if i.positive?
      end
      args.outputs.primitives << credits_window.render_hash
      args.outputs.primitives << credits_elements.map(&:render_hash)
    end
  end

  def render_end_screen
    return unless state.checkmate

    end_window = Window.new(530, 300, 220, 'Checkmate')
    end_elements = [
      Textbox.new(10, 10, 200, state.current_player == 0 ? 'You won!' : 'You lost'),
      Button.new(10, 10, 200, 30, 'Restart')
    ]
    end_elements.each do |elem|
      elem.h = 35 if elem.type == 'textbox'
      end_window.h += elem.h + 5
    end
    end_window.h += 15
    pos_y = 0
    end_elements.each_with_index do |elem, i|
      elem.x += end_window.x
      elem.y = (end_window.y + end_window.h) - pos_y - 5
      handle_end_actions(elem) if elem.type == 'button'
      end_window.add_element(elem)
      pos_y += elem.h + 40 if i.zero?
      pos_y += elem.h if i.positive?
    end
    args.outputs.primitives << end_window.render_hash
    args.outputs.primitives << end_elements.map(&:render_hash)
  end

  def handle_end_actions(button)
    return unless inputs.mouse.inside_rect?(button.render_hash[0])

    button.color = { r: 200, g: 200, b: 200 }
    state.restart = true if inputs.mouse.click && button.text == 'Restart'
  end

  def handle_key_events
    if inputs.keyboard.key_down.escape && !state.show_menu
      state.show_menu = true
      state.show_options = false
      state.show_credits = false
    elsif inputs.keyboard.key_down.escape && state.show_menu
      state.show_menu = false
      state.show_options = false
      state.show_credits = false
    end
  end

  def handle_menu_actions(button)
    if inputs.mouse.inside_rect?(button.render_hash[0])
      button.color = { r: 200, g: 200, b: 200 }
      if inputs.mouse.click
        case button.text
        when 'Resume'
          state.show_menu = false
        when 'Options'
          state.show_menu = false
          state.show_options = true
        when 'Restart'
          state.restart = true
        when 'Credits'
          state.show_menu = false
          state.show_credits = true
        when 'Exit'
          exit
        end
        delay_activation(20)
      end
    end
  end

  def handle_options_actions(elem)
    if inputs.mouse.inside_rect?(elem.render_hash[0])
      elem.color = { r: 200, g: 200, b: 200 }
      case elem.type
      when 'button'
        if inputs.mouse.click && check_delay_activation
          case elem.text
          when 'Back'
            state.show_options = false
            state.show_menu = true
          end
        end
      when 'checkbox'
        elem.color = { r: 250, g: 250, b: 250 }
        if inputs.mouse.click && check_delay_activation
          case elem.text
          when 'Moves highlight'
            state.option_highlight = state.option_highlight ? false : true
          when 'Validate movements'
            state.option_validation = state.option_validation ? false : true
          when 'Show threat map'
            state.option_threat_map = state.option_threat_map ? false : true
          end
        end
      end
    end
  end

  def handle_credits_actions(button)
    if inputs.mouse.inside_rect?(button.render_hash[0])
      button.color = { r: 200, g: 200, b: 200 }
      if inputs.mouse.click
        case button.text
        when 'Back'
          state.show_credits = false
          state.show_menu = true
        end
      end
    end
  end

  # Promotion dialog
  # -----------------------------------------------------------------------------
  def render_promotion_dialog
    return unless state.can_promote && !state.promotion_candidate.nil?

    board = state.game_board.map { |row| row.map(&:clone) }
    piece = board[state.promotion_candidate.y][state.promotion_candidate.x]
    if ((piece.type == :pawn || piece.type == :lancer) && (state.promotion_candidate.y.zero? || state.promotion_candidate.y == 8)) ||
       (piece.type == :knight && (state.promotion_candidate.y < 2 || state.promotion_candidate.y > 6))
      state.turns.last.move.promotion = '+'
      piece.promote
      state.game_board = board
      render_update
      state.can_promote = false
      state.promotion_candidate = nil
    else
      offset_y = state.promotion_candidate.y.zero? ? state.tile_size : (state.tile_size * -1) - 25
      promotion_dialog = ConfirmDialog.new(
        state.board_pos_x + (state.promotion_candidate.x * state.tile_size) - 50,
        state.board_pos_y + (state.promotion_candidate.y * state.tile_size) + offset_y,
        160,
        'Promote?'
      )
      promotion_dialog.elements.each do |elem|
        promotion_confirmation(elem)
      end
      args.outputs.primitives << promotion_dialog.render_hash
    end
  end

  def promotion_confirmation(button)
    if inputs.mouse.inside_rect?(button.render_hash[0])
      button.color = { r: 200, g: 200, b: 200 }
      if inputs.mouse.click
        case button.text
        when 'Yes'
          state.turns.last.move.promotion = '+'
          board = state.game_board.map { |row| row.map(&:clone) }
          board[state.promotion_candidate.y][state.promotion_candidate.x].promote
          state.game_board = board
          render_update
          state.can_promote = false
          state.promotion_candidate = nil
        when 'No'
          state.can_promote = false
          state.promotion_candidate = nil
        end
        render_update
      end
    end
  end

end
