# Shogi

This is a personal side project that I started in the hopes of learning game development. It is done with [DragonRuby](https://dragonruby.org).

<img src="dr_shogi_game.jpg">

It is a Shogi board game that can be used to play, or to reproduce matches. By default, all moves are validated, so you cannot do any illegal move. This feature can be disabled in the game options. Press the `Esc` key to show the menu.

# Requirements

You need a version of the DragonRuby engine to run this project. This is only the project folder.

# Usage and installation

Move to your DragonRuby engine folder and run these commands:

```bash
git clone https://gitlab.com/computer045/dr-shogi.git shogi/
./dragonruby shogi
```

# Contribution

Contributions are welcome. I will look at all of the merge requests.

# License

Licensed under the MIT Licence. For more information read the `LICENSE.txt` file.
