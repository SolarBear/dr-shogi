# frozen_string_literal: true

## Definition of class Button
# Serves to create a button for menus and windows.
class Button < WindowElement
  attr_reader :text
  attr_accessor :color

  def initialize(pos_x, pos_y, width, height, text)
    super(pos_x, pos_y, width, height)
    @text = text
    @color = { r: 230, g: 230, b: 230 }
    @font = 'fonts/monogram.ttf'
  end

  def render_hash
    [
      { x: @x, y: @y, w: @w, h: @h, r: @color.r, g: @color.g, b: @color.b, a: 255 }.solid!,
      { x: @x, y: @y, w: @w, h: @h }.border!,
      { x: @x + (@w / 2), y: (@y + @h) - 2, alignment_enum: 1, text: @text, font: @font, size_enum: 1 }.label!
    ]
  end

  def type
    'button'
  end
end
