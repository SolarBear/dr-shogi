# frozen_string_literal: true

## Definition of class ConfirmDialog
# Serves to create a confirm dialog.
class ConfirmDialog
  attr_reader :x, :y, :w, :elements
  attr_accessor :h

  def initialize(pos_x, pos_y, width, prompt)
    @x = pos_x
    @y = pos_y
    @w = width
    @h = 0
    @prompt = prompt
    @font = 'fonts/monogram.ttf'
    @elements = [
      Button.new(pos_x + 10, pos_y + 10, (width / 2).to_i - 10, 30, 'Yes'),
      Button.new(pos_x + (width / 2).to_i + 5, pos_y + 10, (width / 2).to_i - 15, 30, 'No')
    ]
    @elements.each do |button|
      @h += button.h
    end
  end

  def serialize
    {
      x: @x,
      y: @y,
      w: @w,
      h: @h,
      prompt: @prompt
    }
  end

  def inspect
    serialize.to_s
  end

  def to_s
    serialize.to_s
  end

  def render_hash
    render_hash = [
      { x: @x, y: @y, w: @w, h: @h + 30, r: 255, g: 255, b: 255, a: 255 }.solid!,
      { x: @x, y: @y, w: @w, h: @h + 30 }.border!,
      { x: (@x + 10), y: (@h + @y + 20), text: @prompt, font: @font, size_enum: 1 }.label!
    ]
    @elements.each do |elem|
      render_hash.push(elem.render_hash)
    end
    render_hash
  end

end
