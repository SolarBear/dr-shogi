class Player
  attr_reader :captured_pieces, :id, :is_jewel

  def initialize(id, is_jewel = false)
    @id = id
    @is_jewel = is_jewel
    @captured_pieces = []
  end

  def serialize
    {
      id: @id
    }
  end

  def inspect
    serialize.to_s
  end

  def to_s
    serialize.to_s
  end

  def capture(piece)
    piece.change_player(self)
    piece.is_promoted = false
    @captured_pieces.push(piece)
  end

  def drop(piece_type)
    @captured_pieces.delete_at(select_capture_piece(piece_type))
  end

  def select_capture_piece(piece_type)
    @captured_pieces.find_index { |piece| piece.type == piece_type }
  end

  def capture_count(piece_type)
    count = 0
    @captured_pieces.each do |piece|
      count += 1 if piece.type == piece_type
    end
    count
  end
end
