# frozen_string_literal: true

## Definition of class Button
# Serves to create a button for menus and windows.
class Checkbox < WindowElement
  attr_reader :text
  attr_accessor :color, :checked

  def initialize(pos_x, pos_y, width, height, text, checked = false)
    super(pos_x, pos_y, width, height)
    @text = text
    @color = { r: 255, g: 255, b: 255 }
    @checked = checked
    @font = 'fonts/monogram.ttf'
  end

  def render_hash
    render_hash = [
      { x: @x, y: @y, w: @w, h: @h, r: 255, g: 255, b: 255, a: 255 }.solid!,
      { x: @x, y: @y, w: @h, h: @h, r: @color.r, g: @color.g, b: @color.b, a: 255 }.solid!,
      { x: @x, y: @y, w: @h, h: @h }.border!,
      { x: @x + @h + 10, y: (@y + @h) - 2, alignment_enum: 0, text: @text, font: @font, size_enum: 1 }.label!
    ]
    if @checked
      render_hash = render_hash.union(
        [
          { x: @x + 5, y: @y + 5, w: @h - 10, h: @h - 10, r: 0, g: 0, b: 0, a: 255 }.solid!
        ]
      )
    end
    render_hash
  end

  def type
    'checkbox'
  end
end
