# frozen_string_literal: true

## Definition of class Camp
# Serves to create a camp for each player to store captured pieces.
class Camp
  attr_reader :player, :layout

  def initialize(args, pos_y, player)
    @x = 0
    @y = pos_y
    @player = player
    @layout = [
      ShogiPiece.new(:pawn, @player, false, args.state.theme),
      ShogiPiece.new(:lancer, @player, false, args.state.theme),
      ShogiPiece.new(:knight, @player, false, args.state.theme),
      ShogiPiece.new(:silver, @player, false, args.state.theme),
      ShogiPiece.new(:gold, @player, false, args.state.theme),
      ShogiPiece.new(:bishop, @player, false, args.state.theme),
      ShogiPiece.new(:rook, @player, false, args.state.theme)
    ]
  end

  def render_hash(args)
    camp_slots = []
    @layout.each_with_index do |piece, idx|
      pos_x = (args.state.board_pos_x + args.state.tile_size) + (idx * args.state.tile_size)
      piece_hash = piece.render_hash(args, pos_x, @y)
      piece_hash.a = 100 if @player.capture_count(piece.type).zero?
      slot =
        [
          {
            x: pos_x,
            y: @y,
            w: args.state.tile_size,
            h: args.state.tile_size,
            r: 100, g: 100, b: 100, a: 255
          }.solid!,
          {
            x: pos_x,
            y: @y,
            w: args.state.tile_size,
            h: args.state.tile_size
          }.border!
        ]
      if args.inputs.mouse.inside_rect?(slot[0]) && @player.capture_count(piece.type).positive?
        slot = slot.union(
          [
            {
              x: pos_x + 1,
              y: @y + 1,
              w: args.state.tile_size - 2,
              h: args.state.tile_size - 2,
              r: 255, g: 255, b: 255, a: 100
            }.solid!
          ]
        )
      end
      if args.state.selected_camp_tile.player_id == @player.id &&
         args.state.selected_camp_tile.pos == idx
        slot = slot.union(
          [
            {
              x: pos_x + 1,
              y: @y + 1,
              w: args.state.tile_size - 2,
              h: args.state.tile_size - 2,
              r: 255, g: 255, b: 255, a: 100
            }.solid!
          ]
        )
      end
      slot = slot.union(
        [
          piece_hash,
          {
            x: (pos_x + args.state.tile_size) - 21,
            y: @y + 1, w: 20, h: 20,
            r: 0, g: 0, b: 0, a: 255
          }.solid!,
          {
            x: (pos_x + args.state.tile_size) - 21,
            y: @y + 1, w: 20, h: 20,
            r: 255, g: 255, b: 255, a: 255
          }.border!,
          {
            x: (pos_x + args.state.tile_size) - 15,
            y: @y + 23, text: @player.capture_count(piece.type).to_s,
            r: 255, g: 255, b: 255, a: 255,
            font: args.state.font
          }.label!
        ]
      )
      camp_slots.push(slot)
    end
    camp_slots
  end

  def serialize
    { camped: @player.captured_pieces, player: @player.id }
  end

  def inspect
    serialize.to_s
  end

  def to_s
    serialize.to_s
  end
end
